const slides = document.querySelectorAll("section");
const slide = document.querySelector("section");
const container = document.querySelector("#panelWrap");
let dur = 0.5;
let offsets = [];
let oldSlide = 0;
let activeSlide = 0;
let iw = window.innerWidth;
let slideWidth = document.querySelector("section").offsetWidth;
let sectionPaddingPx = window.getComputedStyle(slide).paddingLeft;
let sectionPadding = parseInt(sectionPaddingPx, 10);
let startContainerPos = document.querySelector(".title-block").offsetLeft;

let dragMe = Draggable.create(container, {
  type: "x",
  edgeResistance: 1,
  snap: offsets,
  inertia: true,  
  bounds: "#masterWrap",
  onDrag: tweenDot,
  onThrowUpdate: tweenDot,
  onDragEnd: slideAnim,
  allowNativeTouchScrolling: false, 
  zIndexBoost: false
});

dragMe[0].id = "dragger";
sizeIt();

function slideAnim(e) {
  oldSlide = activeSlide;

  if (this.id === "dragger") {
    activeSlide = offsets.indexOf(this.endX);
    activeSlide = e.deltaY > 0 ? (activeSlide += 1) : (activeSlide -= 1);
  }

  activeSlide = activeSlide < 0 ? 0 : activeSlide;
  activeSlide = activeSlide > slides.length - 1 ? slides.length - 1 : activeSlide;
  if (oldSlide === activeSlide) {
    return;
  }

  if (this.id != "dragger") {
    gsap.to(container, dur, { x: offsets[activeSlide], onUpdate: tweenDot });
  }
}

function sizeIt() {
  offsets = [];
  gsap.set("#panelWrap", { width: slides.length * slideWidth + startContainerPos });
  gsap.set(slides, { width: slideWidth });
  for (let i = 0; i < slides.length; i++) {
    offsets.push(-slides[i].offsetLeft );
  }
  gsap.set(container, { x: offsets[slideWidth] });
  dragMe[0].vars.snap = offsets[slideWidth];

  startSliderPosition();
}

window.addEventListener("wheel", slideAnim);
window.addEventListener("resize", sizeIt);

function tweenDot() {
  const dotAnim = gsap.timeline({ paused: true });
  gsap.set(dotAnim, {
    time: Math.abs(gsap.getProperty(container, "x") / iw) + 1
  });
}

function startSliderPosition() {
  document.querySelector("#panelWrap").style.paddingLeft = startContainerPos + "px";
}